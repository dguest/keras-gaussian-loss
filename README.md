DumbNet Trainer
===============

This trains a very simple NN that has a Gaussian likelihood loss. It will predict the mean and "precision" of a Gaussian likelihood, where $\text{precision} = 1/\sigma^2$.

The tricky part was making the loss function.
