#!/usr/bin/env python3

import numpy as np
import tensorflow.keras as keras
from tensorflow.keras import backend as K
import tensorflow as tf

def get_dataset(size=1000000):
    targets = np.random.random(size)
    inputs = targets + 0.4 * np.random.random(size) * targets
    return inputs, targets

def gaussian_loss(targ, pred):
    z = pred[:,0:1]
    sigma = pred[:,1:2]
    loss = K.log(K.square(sigma)) + (
        K.square(z - targ) * K.pow(sigma,-2))
    return loss

def gaussian_loss_prec(targ, pred):
    z = pred[:,0:1]
    prec = K.abs(pred[:,1:2])
    loss = - K.log(prec+1e-30) + K.square(z - targ) * prec
    return loss

def rms_loss(targ, pred):
    z = pred[:,0:1]
    return K.square(targ - z)


def get_model():
    inputs = keras.layers.Input(shape=(1))
    x = inputs
    for _ in range(10):
        x = keras.layers.Dense(units=200, activation='relu')(x)
    outputs = keras.layers.Dense(units=2)(x)
    model = keras.Model(inputs=inputs, outputs=outputs)
    print(model.summary())
    model.compile(optimizer=keras.optimizers.Adam(),
                  loss=gaussian_loss_prec)
    return model


def run():
    inputs, targets = get_dataset()
    model = get_model()
    model.fit(inputs, targets, epochs=10)
    print(model.predict([0, 0.5, 1]))

if __name__ == '__main__':
    run()
